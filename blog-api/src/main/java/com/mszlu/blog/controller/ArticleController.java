package com.mszlu.blog.controller;


import com.mszlu.blog.common.cache.Cache;
import com.mszlu.blog.common.aop.LogAnnotation;
import com.mszlu.blog.service.ArticleService;
import com.mszlu.blog.vo.ArticleVo;
import com.mszlu.blog.vo.Result;
import com.mszlu.blog.vo.params.ArticleParam;
import com.mszlu.blog.vo.params.PageParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("articles")
public class ArticleController {
    @Autowired
    private ArticleService articleService;
//首页文章列表

    @PostMapping
    @LogAnnotation(module = "文章",operation = "获取文章列表")
    @Cache(expire = 5 * 60 * 1000,name = "hot_article")

    public Result listArticle(@RequestBody PageParams pageParms){//将获取到的参数 注入当pageParms类中的 成员变量中
        return  articleService.listArticle(pageParms);
    }

    @PostMapping("hot")
    @Cache(expire = 5 * 60 * 1000,name = "hot_article")
    public Result hotArticle(){
        int limit = 5;
        return articleService.hotArticle(limit);
    }

    @PostMapping("new")
    @Cache(expire = 5 * 60 * 1000,name = "hot_article")


    public Result newArticles(){
        int limit=5 ;
        return  articleService.newArticles(limit);
    }


    @PostMapping("listArchives")
    public Result listArchives(){
        return  articleService.listArchives();
    }

    @PostMapping("view/{id}")
    public Result findArticleById(@PathVariable("id") Long id) {
        ArticleVo articleVo = articleService.findArticleById(id);
        return Result.success(articleVo);
    }
    @PostMapping("publish")
    public Result publish(@RequestBody ArticleParam articleParam){
        return articleService.publish(articleParam);
    }
}
