package com.mszlu.blog.service;

import com.mszlu.blog.vo.ArticleVo;
import com.mszlu.blog.vo.Result;
import com.mszlu.blog.vo.params.ArticleParam;
import com.mszlu.blog.vo.params.PageParams;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;



public interface ArticleService {

    //分页查询文章列表
    Result listArticle(PageParams pageParms);
        // 最热文章
    Result hotArticle(int limit);
    //最新文章
    Result newArticles(int limit);
    //文章归档
    Result listArchives();


    ArticleVo findArticleById(Long id);

    Result publish(ArticleParam articleParam);
}
