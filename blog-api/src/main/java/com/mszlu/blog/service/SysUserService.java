package com.mszlu.blog.service;

import com.mszlu.blog.dao.pojo.SysUser;
import com.mszlu.blog.vo.Result;
import com.mszlu.blog.vo.UserVo;


public interface SysUserService {

    SysUser findUserById(Long id);

    SysUser findUser(String account, String pwd);


    //根据token查询用户信息

    Result findUserByToken(String token);

    SysUser findUserByAccount(String account);

    void save(SysUser sysUser);

    Result getUserInfoByToken(String token);

    UserVo findUserVoById(Long id);
}
